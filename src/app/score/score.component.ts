import { Component, OnInit } from '@angular/core';
import {PlayerInfoService} from '../game/player-info.service';
import {PlayerInfo} from '../models/playerInfo';

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.less']
})
export class ScoreComponent implements OnInit {
  playerInfo: PlayerInfo = {keys: 0, gold: 0};

  constructor(
    private playerInfoService: PlayerInfoService
  ) { }

  ngOnInit() {
    this.playerInfoService.playerInfo$.subscribe((playerInfo: PlayerInfo) => this.playerInfo = playerInfo);
  }
}
