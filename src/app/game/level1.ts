import {Level} from './level';
import {ElementType} from './elementType';

class Level1 extends Level {
  readonly w: number = 6;
  readonly h: number = 5;
  readonly startX: number = 1;
  readonly startY: number = 1;
  readonly data: ElementType[][] = [
    [1, 1, 1, 1, 1, 1],
    [1, 0, 2, 1, 4, 1],
    [1, 0, 0, 1, 0, 1],
    [1, 1, 0, 3, 0, 1],
    [1, 1, 1, 1, 1, 1]
  ];
}

export {Level1};
