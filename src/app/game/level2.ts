import {Level} from './level';
import {ElementType} from './elementType';

class Level2 extends Level {
  readonly w: number = 12;
  readonly h: number = 8;
  readonly startX: number = 1;
  readonly startY: number = 6;
  readonly data: ElementType[][] = [
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 2, 1, 0, 0, 0, 2, 1, 0, 4, 1],
    [1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1],
    [1, 0, 1, 4, 0, 1, 1, 0, 1, 1, 0, 1],
    [1, 0, 1, 1, 0, 0, 0, 0, 3, 0, 0, 1],
    [1, 0, 3, 0, 0, 1, 0, 1, 1, 0, 1, 1],
    [1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 4, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  ];
}

export {Level2};
