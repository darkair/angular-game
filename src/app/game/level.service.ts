import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import {Level} from './level';
import {Level1} from './level1';
import {Level2} from './level2';

@Injectable({
  providedIn: 'root'
})
export class LevelService {
  level: Level = null;
  level$: Subject<Level> = new Subject<Level>();

  constructor() { }

  loadLevel(levelId?: number): void {
    if (levelId !== undefined) {
      switch (levelId) {
        default:
        case 1:
          this.level = new Level1();
          break;

        case 2:
          this.level = new Level2();
          break;
      }
    }
    if (this.level === null) {
      this.level = new Level1();
    }
    return this.level$.next(this.level);
  }
}
