export enum ElementType {
    Space = 0,
    Wall = 1,
    Key = 2,
    Door = 3,
    Gold = 4,
    DoorOpen = 5,

    Player = 100,
    Enemy = 101,
}
