import {ElementType} from './elementType';
import {throwError} from 'rxjs';

export abstract class Level {
  readonly w: number;
  readonly h: number;
  readonly startX: number;
  readonly startY: number;
  readonly data: ElementType[][];

  calcAmount(type: ElementType): number {
    return this.data.reduce((a, b) => {
      return a + b.reduce((c, d) => {
        if (d === type) {
          return c + 1;
        }
        return c;
      }, 0);
    }, 0);
  }

  get keys(): number {
    return this.calcAmount(ElementType.Key);
  }

  get gold(): number {
    return this.calcAmount(ElementType.Gold);
  }

  getCell(x: number, y: number): ElementType {
    if (x < 0 || x >= this.w) {
      throwError('X outside of dimentional');
    }
    if (y < 0 || y >= this.h) {
      throwError('Y outside of dimentional');
    }
    return this.data[y][x];
  }

  setCell(x: number, y: number, v: ElementType): void {
    if (x < 0 || x >= this.w) {
      throwError('X outside of dimentional');
    }
    if (y < 0 || y >= this.h) {
      throwError('Y outside of dimentional');
    }
    this.data[y][x] = v;
  }
}
