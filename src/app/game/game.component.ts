import {Component, OnInit} from '@angular/core';
import {LevelService} from './level.service';
import {Player} from '../models/player';
import {PlayerInfo} from '../models/playerInfo';
import {KeyboardService} from '../keyboard.service';
import {AliveObject} from '../models/aliveObject';
import {ElementType} from './elementType';
import {Level} from './level';
import {PlayerInfoService} from './player-info.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.less']
})
export class GameComponent implements OnInit {
  private level: Level;
  private player: Player;
  private currentLevel = 1;
  public levelIsLoaded = false;

  constructor(
    private levelService: LevelService,
    private keyboardService: KeyboardService,
    private playerInfoService: PlayerInfoService
  ) {}

  ngOnInit(): void {
    this.levelService.level$.subscribe(level => {
      this.levelIsLoaded = true;
      this.level = level;
      this.player = new Player(level.startX, level.startY);
      this.player.changesSubject.subscribe((playerInfo: PlayerInfo) => {
        this.playerInfoService.playerInfo$.next(playerInfo);
        if (playerInfo.gold === this.level.calcAmount(ElementType.Gold)) {
          this.currentLevel++;
          this.levelService.loadLevel(this.currentLevel);
        }
      });
    });

    this.levelService.loadLevel(this.currentLevel);
    this.keyboardService.keyActions$.subscribe(e => this.controls(e));
  }

  get levelData(): ElementType[][] {
    const data = JSON.parse(JSON.stringify(this.level.data));
    const {x, y} = this.player.getCoords();
    data[y][x] = ElementType.Player;
    return data;
  }

  private controls(event): void {
    if (this.keyboardService.isLeft(event)) {
      this.move(this.player, -1, 0);
    }
    if (this.keyboardService.isRight(event)) {
      this.move(this.player, +1, 0);
    }
    if (this.keyboardService.isUp(event)) {
      this.move(this.player, 0, -1);
    }
    if (this.keyboardService.isDown(event)) {
      this.move(this.player, 0, +1);
    }
  }

  private move(obj: AliveObject, dx: number, dy: number): boolean {
    const {x, y} = obj.getCoords();
    switch (this.level.getCell(x + dx, y + dy)) {
      case ElementType.Wall:
        return false;

      case ElementType.Door:
        if (obj instanceof Player) {
          if (obj.getKeys() === 0) {
            return false;
          }
          obj.addKeys(-1);
          this.level.setCell(x + dx, y + dy, ElementType.DoorOpen);
          break;
        }
        return false;

      case ElementType.Key:
        if (obj instanceof Player) {
          obj.addKeys(1);
          this.level.setCell(x + dx, y + dy, ElementType.Space);
          break;
        }
        return false;

      case ElementType.Gold:
        if (obj instanceof Player) {
          obj.addGold(1);
          this.level.setCell(x + dx, y + dy, ElementType.Space);
          break;
        }
        return false;
    }

    obj.move(dx, dy);
  }
}
