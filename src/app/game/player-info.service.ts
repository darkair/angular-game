import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {PlayerInfo} from '../models/playerInfo';

@Injectable({
  providedIn: 'root'
})
export class PlayerInfoService {
  playerInfo$: BehaviorSubject<PlayerInfo> = new BehaviorSubject({keys: 0, gold: 0});

  constructor() { }
}
