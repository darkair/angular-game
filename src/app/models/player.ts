import {AliveObject} from './aliveObject';
import {BehaviorSubject} from 'rxjs';
import {PlayerInfo} from './playerInfo';

export class Player extends AliveObject {
  changesSubject: BehaviorSubject<PlayerInfo>;

  private _keys = 0;
  private _gold = 0;

  constructor(_x: number, _y: number) {
    super();

    this.changesSubject = new BehaviorSubject({keys: this._keys, gold: this._gold});

    this.x = _x;
    this.y = _y;
  }

  get keys(): number {
    return this._keys;
  }

  set keys(v: number) {
    this._keys = v;
    this.changesSubject.next({keys: this._keys, gold: this._gold});
  }

  get gold(): number {
    return this._gold;
  }

  set gold(v: number) {
    this._gold = v;
    this.changesSubject.next({keys: this._keys, gold: this._gold});
  }

  addKeys(n: number): number {
    this.keys += n;
    return this.getKeys();
  }

  getKeys(): number {
    return this.keys;
  }

  addGold(n: number): number {
    this.gold += n;
    return this.getGold();
  }

  getGold(): number {
    return this.gold;
  }
}
