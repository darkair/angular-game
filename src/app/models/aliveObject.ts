export class AliveObject {
  protected x: number;
  protected y: number;

  getCoords(): {x: number, y: number}  {
    return {x: this.x, y: this.y};
  }

  move(dx: number, dy: number): void {
    this.x += dx;
    this.y += dy;
  }
}
