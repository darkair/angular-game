import { TestBed } from '@angular/core/testing';

import { UsersFromUrlService } from './users-from-url.service';

describe('UsersFromUrlService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UsersFromUrlService = TestBed.get(UsersFromUrlService);
    expect(service).toBeTruthy();
  });
});
