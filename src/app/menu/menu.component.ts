import { Component, OnInit } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.less']
})
export class MenuComponent implements OnInit {
  constructor(public router: Router) { }
  
  ngOnInit() {
  }

  play(): void {
    console.log('PLAY');
    this.router.navigateByUrl('/play');
  }
}
