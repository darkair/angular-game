import { Injectable } from '@angular/core';
import { Observable, fromEvent, of, merge } from 'rxjs';
import { throttleTime, filter, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class KeyboardService {

  public keyActions$: Observable<KeyboardEvent>;

  constructor() {
    this.keyActions$ = fromEvent(document, 'keydown')
      .pipe(
        map(event => <KeyboardEvent>event),
        // throttleTime(500)
      );
  }

  isLeft(event: KeyboardEvent): boolean {
    return event.keyCode === 37;
  }

  isRight(event: KeyboardEvent): boolean {
    return event.keyCode === 39;
  }

  isUp(event: KeyboardEvent): boolean {
    return event.keyCode === 38;
  }

  isDown(event: KeyboardEvent): boolean {
    return event.keyCode === 40;
  }
}
