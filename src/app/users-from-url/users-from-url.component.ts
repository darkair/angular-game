import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Observable, of, fromEvent} from 'rxjs';
import {UsersFromUrlService} from '../users-from-url.service';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-users-from-url',
  templateUrl: './users-from-url.component.html',
  styleUrls: ['./users-from-url.component.less']
})
export class UsersFromUrlComponent implements OnInit {
  public users: Array<{}> = null;
  private click$: Observable<MouseEvent>;
  @ViewChild('refreshBtn') refreshBtn: ElementRef;

  constructor(
    private usersService: UsersFromUrlService
  ) { }

  ngOnInit() {
    this.click$ = fromEvent(this.refreshBtn.nativeElement, 'click');

    const requestStream = this.click$
      .pipe(
        map(() => {
          const randomOffset = Math.floor(Math.random() * 100);
          return 'https://api.github.com/users?since=' + randomOffset;
        }),
        startWith('https://api.github.com/users')
      );

    requestStream.subscribe(requestUrl => {
      this.usersService.get(requestUrl).subscribe(data => {
        this.users = data;
        console.log(this.users);
      });
    });
  }
}
