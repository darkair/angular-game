import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersFromUrlComponent } from './users-from-url.component';

describe('UsersFromUrlComponent', () => {
  let component: UsersFromUrlComponent;
  let fixture: ComponentFixture<UsersFromUrlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersFromUrlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersFromUrlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
